# -*- coding: utf-8 -*-
import time
import os
from odoo import models, fields, _, api
from odoo.tools.safe_eval import safe_eval
from odoo.exceptions import ValidationError


class MassProductCreation(models.Model):
    _name = 'mass.product.creation'

    def get_default_year(self):
        return time.strftime("%Y")

    customer_id = fields.Many2one('res.partner', string='Customer',
                                  required=True)
    design_name = fields.Char(string='Design Name', size=15, required=True)
    design_year = fields.Char(string='Design Year',  size=4,
                              default=get_default_year, required=True)
    internal_category_id = fields.Many2one('product.category',
                                        string='Internal Category')
    product_type = fields.Many2one('mass.product.type.category',
                                   string='Product Type', required=True)

    product_line_ids = fields.One2many('mass.product.creation.product.line',
                                   'product_mass_id', string='Products', required=True)

    # @api.onchange('design_name')
    # def _onchange_design_name(self):
    #     if self.design_name:
    #         self.design_name = self.design_name.upper().replace(' ', '')
    @api.model
    def create(self, vals):
        error = False
        if not vals.get('product_line_ids'):
            error = _('Error! please add products to the list.')
        if error:
            raise ValidationError(_(error))
        return super(MassProductCreation, self).create(vals)

    def apply_change(self):
        product_line_ids = []

        for product_line in self.product_line_ids:
            product = product_line.product_type_id
            product_line_ids.append({
                'product_name': self.format_product_name(product, product_line),
                'product_type_id': product.id,
                'product_variant_id': product.product_type_attribute.id,
                'customer_id': self.customer_id.id or False,
                'product_abbreviation': product.product_abbreviation or '',
                'internal_reference': self.format_internal_reference(
                    product, product_line),
                'product_variant_value_id': product_line.product_variant_value_id.id or False,
                'notes': product_line.notes or ''
            })

        self.product_line_ids = product_line_ids

    @api.onchange('customer_id')
    def _onchange_customer_id(self):
        if self.customer_id:
            self.apply_change()

    @api.onchange('design_name')
    def _onchange_design_name(self):
        if self.design_name:
            self.apply_change()

    @api.onchange('design_year')
    def _onchange_design_year(self):
        if self.design_year:
            self.apply_change()

    @api.onchange('product_type')
    def _onchange_product_type(self):
        if self.product_type:
            products = self.env['mass.product.type'].\
                search([('product_type_category_id', '=', self.product_type.id)])
            product_line_ids = []

            for product in products:
                product_line_ids.append({
                    'product_name': self.format_product_name(product),
                    'product_type_id': product.id,
                    'product_variant_id': product.product_type_attribute.id,
                    'customer_id': self.customer_id.id or False,
                    'product_abbreviation': product.product_abbreviation or '',
                    'notes': product.notes or '',
                    'internal_reference': self.format_internal_reference(product)
                })

            self.product_line_ids = product_line_ids

    def format_product_name(self, product, product_line=False):
        name = self.design_year
        if product_line and product_line.product_variant_value_id:
            name = '%s %s' % (name, product_line.product_variant_value_id.name)
        return '%s %s' % (name, product.name or '')

    def format_internal_reference(self, product, product_line=False):
        name = product.product_abbreviation
        if product_line and product_line.product_variant_value_id:
            name = '%s-%s' % (name, product_line.product_variant_value_id.name)
        internal_reference = '%s-%s-%s-%s' % (
            name,
            self.customer_id.name or '',
            self.design_year[-2:] or '',
            self.design_name or ''
        )

        return internal_reference.replace(' ', '').upper()


class ProductLine(models.Model):
    _name = 'mass.product.creation.product.line'

    product_mass_id = fields.Many2one('mass.product.creation',
                                      string='Product Mass Reference',
                                      required=True, ondelete='cascade',
                                      index=True, copy=False)
    product_type_id = fields.Many2one('mass.product.type',
                                 string='Product Type', required=True)

    customer_id = fields.Many2one('res.partner', string='Customer',
                                  required=True)
    product_variant_id = fields.Many2one('product.attribut',
                                         string='Variant')
    product_variant_value_id = fields.Many2one('product.attribute.value', string='Variant Value', required=True)
    product_name = fields.Char(string='Product Name')
    product_abbreviation = fields.Char(string='Product Abbreviation')
    notes = fields.Char(string='Notes')
    internal_reference = fields.Char(string='Internal Reference')

    @api.model
    def create(self, vals):
        error = False
        if not vals.get('product_variant_value_id'):
            error = _('Error! please choose variants for all products in the below list!')
        if error:
            raise ValidationError(_(error))
        return super(ProductLine, self).create(vals)

    @api.model
    def get_prices(self):
        try:
            import pandas as pd
            import io
            import base64

            IrConfigParam = self.env['ir.config_parameter']
            toread = io.BytesIO()
            file = base64.b64decode(safe_eval(
                IrConfigParam.get_param(
                    'mass_product_creation.master_pricelist', 'False')))
            toread.write(
                file)  # pass your `decrypted` string as the argument here
            toread.seek(0)  # reset the pointer

            xlsx = pd.ExcelFile(toread)
            pricelists = self.env['product.pricelist'].search(
                [('is_mass', '=', True)])
            prices = {}
            for pricelist in pricelists:

                sheetX = xlsx.parse(pricelist.xlsx_index)  # 2 is the sheet number
                products_list = sheetX.values.tolist()
                for item in products_list:
                    if item[0] and item[1]:
                        key = 'pricelist%s$$$-%s-%s' % (pricelist.id, item[0].splitlines()[0], item[1])
                        prices[key.lower().replace(' ', '-')] = item
            return prices

        except ImportError:
            return False

    @api.multi
    def mass_create(self):
        products = []
        customer = False
        pricelists = self.env['product.pricelist'].search([('is_mass', '=', True)])
        prices = self.get_prices()

        for product in self:
            customer = product.product_mass_id.customer_id
            existing_product = self.env['product.product'].\
                search([('default_code', '=', product.internal_reference), ('customer_id', '=', customer.id)], limit=1)
            if not existing_product:
                new_product = self.env['product.product'].create({
                    'name': product.product_name,
                    'sale_ok': product.product_type_id.sale_ok,
                    'purchase_ok': product.product_type_id.purchase_ok,
                    'type': product.product_type_id.type,
                    'categ_id': product.product_mass_id.internal_category_id.id,
                    'standard_price': product.product_type_id.standard_price,
                    'income_account_id': product.product_type_id.income_account_id.id,
                    'route_ids': product.product_type_id.route_ids,
                    'weight': product.product_type_id.weight,
                    'volume': product.product_type_id.volume,
                    'description_sale': product.notes,
                    'default_code': product.internal_reference,
                    'customer_id': customer.id,
                })

                for pricelist in pricelists:
                    key = 'pricelist%s$$$-%s-%s' % (pricelist.id, product.product_type_id.name,
                                     product.product_type_id.product_abbreviation)
                    key = key.lower().replace(' ', '-')

                    bands = pricelist.mmq.split(",")

                    if prices[key]:
                        self.env['product.pricelist.item'].create({
                            'product_id': new_product.id,
                            'min_quantity': 1,
                            'pricelist_id': pricelist.id,
                            'fixed_price': prices[key][2],
                            'product_tmpl_id': new_product.product_tmpl_id.id
                        })
                        self.env['product.pricelist.item'].create({
                            'product_id': new_product.id,
                            'min_quantity': bands[0] or 0,
                            'pricelist_id': pricelist.id,
                            'fixed_price': prices[key][3],
                            'product_tmpl_id': new_product.product_tmpl_id.id
                        })
                        self.env['product.pricelist.item'].create({
                            'product_id': new_product.id,
                            'min_quantity': bands[1] or 0,
                            'pricelist_id': pricelist.id,
                            'fixed_price': prices[key][4],
                            'product_tmpl_id': new_product.product_tmpl_id.id
                        })

                new_product.product_tmpl_id.write({
                    'customer_id': customer.id
                })

                products.append((0, 0, {
                    'product_id': new_product.id,
                    'product_qty': 1,
                })
                )
        order = self.env['sale.order'].create({
            'partner_invoice_id': customer.id,
            'partner_shipping_id': customer.id,
            'partner_id': customer.id,
            'pricelist_id': customer.property_product_pricelist.id,
            'order_line': products
        })

        view = self.env.ref('sale.view_order_form')

        return {
            'type': 'ir.actions.act_window',
            'res_model': 'sale.order',
            'target': 'current',
            'view_type': 'form',
            'view_mode': 'form',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'res_id': order.id
        }

    @api.onchange('product_variant_value_id')
    def _onchange_product_variant_value_id(self):
        if self.product_variant_value_id:
            self.product_name = '%s %s %s' % (
                self.product_mass_id.design_year or '',
                self.product_variant_value_id.name or '',
                self.product_type_id.name or ''
            )
            self.internal_reference = '%s-%s-%s-%s-%s' % (
                self.product_type_id.product_abbreviation or '',
                self.product_variant_value_id.name or '',
                self.customer_id.name or '',
                self.product_mass_id.design_year[-2:] or '',
                self.product_mass_id.design_name or ''
            )
            self.internal_reference = self.internal_reference.replace(' ', '').\
                upper()


class ProductType(models.Model):

    _name = 'mass.product.type'
    _description = "Product Type"
    _inherits = {'product.template': 'product_tmpl_id'}
    _inherit = ['mail.thread']
    _order = 'default_code, name, id'

    product_tmpl_id = fields.Many2one(
        'product.template', 'Product Template',
        auto_join=True, index=True, ondelete="cascade", required=True)
    product_type_category_id = fields.Many2one('mass.product.type.category',
                                         string='Product Type Category',
                                         required=True)

    product_abbreviation = fields.Char(string='Product Abbreviation')
    notes = fields.Char(string='Notes')

    product_type_attribute = fields.Many2one('product.attribute',
                                             string='Attribute')
    income_account_id = fields.Many2one('account.account',
                                        company_dependent=True,
                                        string="Income Account",
                                        )


class ProductTypeCategory(models.Model):

    _name = 'mass.product.type.category'

    name = fields.Char(string='Category Name', required=True)


class ResPartner(models.Model):

    _inherit = 'res.partner'

    product_ids = fields.One2many('product.product',
                                  'customer_id', string='Customer Products')


class ProductTemplate(models.Model):

    _inherit = 'product.template'

    customer_id = fields.Many2one('res.partner', string='Customer')

    @api.onchange('customer_id')
    def _onchange_customer_id(self):
        self.env['product.product'].search([
            ('default_code', '=', self.default_code),
            ('product_tmpl_id', '=', self._origin.id)]
        ).write({'customer_id': self.customer_id.id})


class ProductProduct(models.Model):

    _inherit = 'product.product'

    customer_id = fields.Many2one('res.partner', string='Customer')

    @api.onchange('customer_id')
    def _onchange_customer_id(self):
        self.product_tmpl_id.write({'customer_id': self.customer_id.id})

    @api.multi
    def mass_create_customer(self):
        pass
        products = []
        customer = False
        for product in self:
            customer = product.customer_id

            products.append((0, 0, {
                'product_id': product.id,
                'product_qty': 1,
            })
                            )
        order = self.env['sale.order'].create({
            'partner_invoice_id': customer.id,
            'partner_shipping_id': customer.id,
            'partner_id': customer.id,
            'pricelist_id': self.env.ref('product.list0').id,
            'order_line': products
        })

        view = self.env.ref('sale.view_order_form')

        return {
            'type': 'ir.actions.act_window',
            'res_model': 'sale.order',
            'target': 'current',
            'view_type': 'form',
            'view_mode': 'form',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'res_id': order.id
        }


class ProductConfigSettings(models.TransientModel):

    _name = 'sale.config.settings'
    _inherit = ['sale.config.settings']

    master_pricelist = fields.Binary('Master Pricelist', store=True)
    master_pricelist_filename = fields.Char('Master Pricelist Filename')


    @api.model
    def get_default_master_pricelist(self, fields):
        IrConfigParam = self.env['ir.config_parameter']
        # we use safe_eval on the result, since the value of the parameter is a nonempty string
        return {
            'master_pricelist': safe_eval(
                IrConfigParam.get_param('mass_product_creation.master_pricelist', 'False')),
            'master_pricelist_filename': safe_eval(
                IrConfigParam.get_param(
                    'mass_product_creation.master_pricelist_filename', 'False')),
        }

    @api.multi
    def set_master_pricelist(self):
        self.ensure_one()
        IrConfigParam = self.env['ir.config_parameter']
        # we store the repr of the values, since the value of the parameter is a required string
        IrConfigParam.set_param('mass_product_creation.master_pricelist', repr(self.master_pricelist))
        IrConfigParam.set_param('mass_product_creation.master_pricelist_filename',
                                repr(self.master_pricelist_filename))


class ProductPricelist(models.Model):

    _inherit = 'product.pricelist'

    min_quantity = fields.Integer('Minimum Quntity')
    xlsx_index = fields.Integer('Excel Index')
    mmq = fields.Char('MMQ')
    is_mass = fields.Boolean('Is mass')
