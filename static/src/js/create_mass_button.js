odoo.define('mass_product_create.create_button', function(require) {

    "use strict";

    var core = require('web.core');
    var KanbanView = require('web_kanban.KanbanView');
    var FormView = require('web.FormView');
    var form_common = require('web.form_common');
    var _t = core._t;
    var QWeb = core.qweb;
    var Model = require('web.Model');
    var FieldOne2Many = core.form_widget_registry.get('one2many');
    var base = require('web_editor.base');

    KanbanView.include({
        render_buttons: function($node) {

            this._super.apply(this, arguments);

            if (this.$buttons) {
                if (this.model == 'product.template') {
                    this.$buttons.append('<button class="btn btn-default btn-sm o_create_mass_product_button" type="button">Mass Create</button>')
                    let new_button = this.$buttons.find('.o_create_mass_product_button');
                    new_button && new_button.click(this.proxy('create_mass_product_button'));
                }
            }

        },

        create_mass_product_button: function() {
            var self = this;
            this.do_action({
                type: 'ir.actions.act_window',
                res_model: 'mass.product.creation',
                res_id: false,
                views: [
                    [false, 'form']
                ],
                target: 'current',
                on_reverse_breadcrumb: function() {
                    return self.reload();
                }
            });
        }

    });


    FormView.include({
        toggle_buttons: function() {

            this._super.apply(this, arguments);
            if (this.$buttons && this.get("actual_mode") == 'create') {
                if (this.model == 'mass.product.creation' && this.$buttons.find('.o_form_buttons_edit button:contains("Save & Create Quote")').length < 1) {
                    this.$buttons.find('.o_form_buttons_edit').append('<button class="btn btn-primary btn-sm o_create_mass_product_button" type="button">Save & Create Quote</button>')
                    let new_button = this.$buttons.find('.o_create_mass_product_button');
                    new_button && new_button.click(this.proxy('save_create_button'));
                }
            } else {
                if(this.$buttons){
                    this.$buttons.find('.o_form_buttons_edit button:contains("Save & Create Quote")').remove();
                }
            }
        },
        save_create_button: function() {
            $('.o_form_button_save').trigger('click');

            var self = this;
            core.bus.on('do_reload_needaction', this, function() {
                var model_obj = new Model('mass.product.creation.product.line');
                model_obj.call('mass_create', [self.datarecord.product_line_ids], {
                        context: self.dataset.context
                    })
                    .then(function(result) {
                        self.do_action(result);
                    });

            });
        }

    });

base.ready().always(function () {
    QWeb.add_template('/mass_product_creation/static/src/xml/list_view.xml');
});

    var One2ManySelectable = FieldOne2Many.extend({
        // my custom template for unique char field
        template: 'One2ManySelectable',

        multi_selection: true,
        //button click
        events: {
            "click .cf_button_confirm": "action_selected_lines",
        },
        start: function() {
            this._super.apply(this, arguments);
            var self = this;
        },
        //passing ids to function
        action_selected_lines: function() {
            var self = this;
            var selected_ids = self.get_selected_ids_one2many();
            if (selected_ids.length === 0) {
                this.do_warn(_t("You must choose at least one record."));
                return false;
            }
            var model_obj = new Model(this.dataset.model);
            //you can hardcode model name as: new Model("module.model_name");
            //you can change the function name below
            /* you can use the python function to get the IDS
                  @api.multi
                def bulk_verify(self):
                        for record in self:
                            print record
            */
            model_obj.call('mass_create_customer', [selected_ids], {
                    context: self.dataset.context
                })
                .then(function(result) {
                    console.log(result);
                    self.do_action(result);
                });
        },
        //collecting the selected IDS from one2manay list
        get_selected_ids_one2many: function() {
            var ids = [];
            this.$el.find('td.o_list_record_selector input:checked')
                .closest('tr').each(function() {

                    ids.push(parseInt($(this).context.dataset.id));
                    console.log(ids);
                });
            return ids;
        },


    });
    // register unique widget, because Odoo does not know anything about it
    //you can use <field name="One2many_ids" widget="x2many_selectable"> for call this widget
    core.form_widget_registry.add('one2many_selectable', One2ManySelectable);


})