# -*- coding: utf-8 -*-
{
    'name': "mass_product_creation",

    'summary': """
        To reduce the time is takes to create a set of products and a first quote for a customer.""",

    'description': """
       Kartdavid designs & manufacturers graphics for karting & motocross. Most of the products we sell are custom and made to order. As a result we have to create many custom products for each customer, typically 5-10 for each design they have. This takes a considerable amount of time and can result in mistakes entering our product data, causing errors in manufacturing & packing.
    """,

    'author': "Fayez Qandeel",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'product',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale', 'product', 'web', 'stock'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'data/pricelists.xml',
        'views/assets.xml',
        'views/views.xml',
        'views/templates.xml',
    ],
    "qweb": [
        'static/src/xml/list_view.xml',
    ],
    "external_dependencies": {
        'python': ['pandas']
    },
    "auto_install": False,
    "installable": True,
    "application": False,
}